﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     17th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="JsonHelper.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class provides helpers for json serialization.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance.TelemetryTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Class which provides helpers for json deserialization.
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// Deserialize json to dictionaries of strings and objects.
        /// </summary>
        /// <param name="json">Json string to be deserialized.</param>
        /// <returns>Deserialized object.</returns>
        public static object Deserialize(string json)
        {
            return ToObject(JToken.Parse(json));
        }

        /// <summary>
        /// Recursively parse Json token.
        /// </summary>
        /// <param name="token">Json token to be parsed.</param>
        /// <returns>Parsed object.</returns>
        public static object ToObject(JToken token)
        {
            if (token == null)
            {
                return null;
            }

            switch (token.Type)
            {
                case JTokenType.Object:
                    return token.Children<JProperty>().ToDictionary(prop => prop.Name, prop => ToObject(prop.Value));
                case JTokenType.Array:
                    return token.Select(ToObject).ToList();

                default:
                    return ((JValue)token).Value;
            }
        } 
    }
}
