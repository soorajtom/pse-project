﻿//-----------------------------------------------------------------------
// <author> 
//     Suman Saurav Panda
// </author>
//
// <date> 
//     18-Nov-2018 
// </date>
// 
// <reviewer> 
//     Amish Ranjan
// </reviewer>
// 
// <copyright file="StubImageCommunication.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is a class intended to provide stub for ImageCommunication
// </summary>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Net;

    /// <summary>
    /// this class is inteneded to deliver stub of image communication to receive image class
    /// </summary>
    public class StubImageCommunication
    {
        /// <summary>
        /// dummy delegate for stub image communication
        /// </summary>
        /// <param name="data">data passed to subscriber</param>
        /// <param name="fromIP">from whose IP the data is coming</param>
        public delegate void SignalHandler(string data, IPAddress fromIP);

        /// <summary>
        /// Gets or sets which signal is coming from its calling method
        /// </summary>
        public Signal Signal { get; set; }

        /// <summary>
        /// dummy funciton for subscription to image communication in stub not implemented
        /// </summary>
        /// <param name="signal">dummy signal for subscription</param>
        /// <param name="receivalHandler">which function to handle upon event raise</param>
        public void SubscribeForSignalReceival(Signal signal, SignalHandler receivalHandler)
        {
            return;
        }

        /// <summary>
        /// this method tells the driver whether in this stub correct signal has set or not
        /// </summary>
        /// <param name="clientIP">takes dummy client IP</param>
        /// <param name="signal">takes the original signal passed by receive image object</param>
        public void SignalImageModule(IPAddress clientIP, Signal signal)
        {
            this.Signal = signal;
        }
    }
}
