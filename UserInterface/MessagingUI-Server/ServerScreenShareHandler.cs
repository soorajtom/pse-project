﻿//-----------------------------------------------------------------------
// <author>
//      A Vinay Krishna
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Change UI layout on screen share start.
// </summary>
// <copyright file="ServerScreenShareHandler.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Masti.QualityAssurance;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Share screen on click function.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void ShareScreenButtonClick(object sender, EventArgs e)
        {
            string clientIP = null;
            try
            {
                clientIP = GetClientName(ServerChatSectionTabs.SelectedTab.Name);
            }
            catch (Exception exception)
            {
                MastiDiagnostics.LogWarning("No client to have screen sharing.");
                MessageBox.Show("No client to have screen sharing.", "Screen share", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            TabPage messagePage = this.GetTabPage(clientIP + "tabPage");
            SplitContainer splitContainer = GetControl(messagePage.Controls, clientIP + "split") as SplitContainer;
            PictureBox currentImageBox = GetControl(splitContainer.Panel1.Controls, clientIP + "pictureBox") as PictureBox;
            TextBox currentTime = GetControl(splitContainer.Panel1.Controls, clientIP + "timeStamp") as TextBox;

            bool isFront = currentImageBox.Visible;

            //// Starts screen share.
            if (!isFront)
            {
                this.imageHandler.GetSharedScreen(clientIP);
                splitContainer.Panel1Collapsed = false;
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\stopScreen.png");
                currentTime.Visible = true;
                currentImageBox.Visible = true;
                this.previousScreenSharingClient = messagePage;
            }
            //// Stops screen share.
            else
            {
                this.imageHandler.StopSharedScreen(clientIP);
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\startScreen.png");
                currentTime.Visible = false;
                currentImageBox.Visible = false;
                splitContainer.Panel1Collapsed = true;
            }
        }
    }
}
