﻿//-----------------------------------------------------------------------
// <author>
//      Polu Varshith
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Save and delete chat sessions.
// </summary>
// <copyright file="ServerPersistance.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows.Forms;
    using Masti.QualityAssurance;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Deletes session.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void DeleteSessionButtonClick(object sender, EventArgs e)
        {
            DialogResult confirmDeleteResult = MessageBox.Show("Do you really want to delete the sessions?", "Delete Session", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmDeleteResult.Equals(DialogResult.OK))
            {
                // Do something.
                int fromSessionID = int.Parse(fromSessionTextBox.Text, CultureInfo.InvariantCulture);
                int toSessionID = int.Parse(toSessionTextBox.Text, CultureInfo.InvariantCulture);
                this.messageHandler.DeleteMessage(fromSessionID, toSessionID);
                MessageBox.Show("From: " + fromSessionID + " To: " + toSessionID + " Sessions deleted.", "Delete Session", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MastiDiagnostics.LogInfo("From: " + fromSessionID + " To: " + toSessionID + " Sessions deleted.");
            }
        }

        /// <summary>
        /// Retrieve session.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void RetrieveSessionButtonClick(object sender, EventArgs e)
        {
            // Do something.
            int fromSessionID = int.Parse(fromSessionTextBox.Text, CultureInfo.InvariantCulture);
            int toSessionID = int.Parse(toSessionTextBox.Text, CultureInfo.InvariantCulture);
            this.messageHandler.RetrieveMessage(fromSessionID, toSessionID);

            string fileName = "User/NaaIshtam.txt";
            MastiDiagnostics.LogInfo("From: " + " To: " + " Sessions retreived to the following file." + fileName);
            MessageBox.Show("Sessions retreived to the following file." + fileName, "Retrieve Session", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Save session.
        /// </summary>
        /// <returns>Dictionary with clientname and their corresponding chat section.<see cref="TextBox"/></returns>
        private Dictionary<string, string> SaveSession()
        {
            Dictionary<string, string> serverChatSessionContent = new Dictionary<string, string>();
            foreach (TabPage page in ServerChatSectionTabs.TabPages)
            {
                string clientIP = GetClientName(page.Name);
                string clientName = page.Text;
                string clientSessionMessage = "";
                SplitContainer splitContainer = GetControl(page.Controls, clientIP + "split") as SplitContainer;
                foreach (Control control in splitContainer.Panel2.Controls)
                {
                    if (control is RichTextBox)
                    {
                        if (control.Location.X == 0)
                        {
                            clientSessionMessage += clientName + ": " + control.Text + "\n";
                        }
                        else
                        {
                            clientSessionMessage += "Me: " + control.Text + "\n";
                        }
                    }
                }
                serverChatSessionContent.Add(clientIP + clientName, clientSessionMessage);
            }
            Console.WriteLine("Dictionary:\n");
            foreach(string key in serverChatSessionContent.Keys)
            {
                Console.WriteLine(key + ": " + serverChatSessionContent[key]);
            }
            return serverChatSessionContent;
        }
    }
}
